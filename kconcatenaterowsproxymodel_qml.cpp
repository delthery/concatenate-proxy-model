#include <qqml.h>
#include <qcoreapplication.h>
#include "kconcatenaterowsproxymodel_qml.h"

namespace qqsfpm {

ConcatenateTablesProxyModelQml::ConcatenateTablesProxyModelQml(QObject *parent)
    : QConcatenateTablesProxyModel(parent)
{
}

//ConcatenateTablesProxyModelQml::~ConcatenateTablesProxyModelQml()
//{
//}

QQmlListProperty<QAbstractItemModel> ConcatenateTablesProxyModelQml::sources()
{
    return QQmlListProperty<QAbstractItemModel>(this,
                                                NULL,
                                                &ConcatenateTablesProxyModelQml::appendSource,
                                                &ConcatenateTablesProxyModelQml::sourceCount,
                                                &ConcatenateTablesProxyModelQml::source,
                                                &ConcatenateTablesProxyModelQml::clear);
}

void ConcatenateTablesProxyModelQml::log()
{
    qDebug() << roleNames();
}

void ConcatenateTablesProxyModelQml::appendSource(QQmlListProperty<QAbstractItemModel> *list, QAbstractItemModel *newItem)
{
    auto q = static_cast<QConcatenateTablesProxyModel *>(list->data);
    q->addSourceModel(newItem);
}

int ConcatenateTablesProxyModelQml::sourceCount(QQmlListProperty<QAbstractItemModel> *list)
{
    auto q = static_cast<QConcatenateTablesProxyModel *>(list->data);
    return q->sourceModels().count();
}

QAbstractItemModel *ConcatenateTablesProxyModelQml::source(QQmlListProperty<QAbstractItemModel> *list, int index)
{
    auto q = static_cast<QConcatenateTablesProxyModel *>(list->data);
    return q->sourceModels().at(index);
}

void ConcatenateTablesProxyModelQml::clear(QQmlListProperty<QAbstractItemModel>* list)
{
    auto q = static_cast<QConcatenateTablesProxyModel *>(list->data);
    if(!q) return;
    const auto sources = q->sourceModels();
    for (auto s : sources) {
        q->removeSourceModel(s);
    }
}

QHash<int, QByteArray> ConcatenateTablesProxyModelQml::roleNames() const
{
    auto sources = sourceModels();
    if (sources.isEmpty()) {
        return {};
    }
    return sources.at(0)->roleNames();
}

void registerConcatenateTablesProxyModelQmlTypes() {
    qmlRegisterType<ConcatenateTablesProxyModelQml>("ConcatenateTablesProxyModel", 0, 1, "ConcatenateTablesProxyModel");
}

Q_COREAPP_STARTUP_FUNCTION(registerConcatenateTablesProxyModelQmlTypes)

}
