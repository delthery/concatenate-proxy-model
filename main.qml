import QtQuick 2.15
import QtQuick.Window 2.15
import QtQuick.Controls 2.12
import SortFilterProxyModel 0.2
import ConcatenateTablesProxyModel 0.1

Window {
    property bool direction: true
    width: 640
    height: 480
    visible: true
    title: qsTr("Hello World")

    ListModel {
        id: firstModel
        ListElement { name: "item 1.1" }
        ListElement { name: "item 1.2" }
        ListElement { name: "item 1.3" }
    }

    ListModel {
        id: secondModel
        ListElement { name: "item 2.1" }
        ListElement { name: "item 2.2" }
        ListElement { name: "item 2.3" }
    }

    ConcatenateTablesProxyModel {
        id: concatModel

        first: firstModel
        second: secondModel


//        sources: [
////            firstModel,
////            secondModel
//            ListModel {
//                id: firstModel2
//                ListElement { name: "item 1.1" }
//                ListElement { name: "item 1.2" }
//                ListElement { name: "item 1.3" }
//            },
//            ListModel {
//                id: secondModel2
//                ListElement { name: "item 2.1" }
//                ListElement { name: "item 2.2" }
//                ListElement { name: "item 2.3" }
//            }
//        ]

        Component.onCompleted: {
            console.log("----------");
            log();
            console.log("----------");
        }
    }

    SortFilterProxyModel {
        id: sortModel
        //sourceModel: firstModel
        sourceModel: concatModel

        sorters: [
            RoleSorter {
                roleName: "name";
                sortOrder: direction ? Qt.DescendingOrder:Qt.AscendingOrder
            }
        ]
    }

    Column {
        anchors.fill: parent
        Button {
            id: filterButton
            text: "Sort"
            onClicked: direction = !direction
        } // Button

        ListView {
            anchors {
                top: filterButton.bottom
                bottom: parent.bottom
            }

            model: sortModel
            //model: concatModel

            delegate: Label {
                text: model && model.name ? model.name : "empty"
//            Component.onCompleted: {
//                console.log(model)
//                console.log(model.display + " " + model.decoration + " " + model.edit)
//            }
            } // Label
        } // ListView
    } // Column
} // Window
