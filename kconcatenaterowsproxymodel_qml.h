#pragma once

#include <QConcatenateTablesProxyModel>

#include <QObject>
#include <QtQml>
#include <QQmlListProperty>

namespace qqsfpm {

class ConcatenateTablesProxyModelQml : public QConcatenateTablesProxyModel
{
    Q_OBJECT
    Q_PROPERTY(QAbstractItemModel* first READ first WRITE setFirst)
    Q_PROPERTY(QAbstractItemModel* second READ second WRITE setSecond)

    using QAIM = QAbstractItemModel;
    Q_PROPERTY(QQmlListProperty<QAIM> sources READ sources)
    Q_CLASSINFO("DefaultProperty", "sources")
    QML_ELEMENT
public:
    explicit ConcatenateTablesProxyModelQml(QObject* parent = nullptr);
//    ~ConcatenateTablesProxyModelQml() override;

    QAbstractItemModel* first() { return _first; }
    void setFirst(QAbstractItemModel* value){
        _first = value;
        addSourceModel(value);
    }
    QAbstractItemModel* second() { return _second; }
    void setSecond(QAbstractItemModel* value){
        _second = value;
        addSourceModel(value);
    }

    QQmlListProperty<QAbstractItemModel> sources();

    Q_INVOKABLE void log();

private:
    static void appendSource(QQmlListProperty<QAbstractItemModel>* list,
                             QAbstractItemModel* newItem);
    static int sourceCount (QQmlListProperty<QAbstractItemModel>* list);
    static QAbstractItemModel* source(QQmlListProperty<QAbstractItemModel>* list, int index);
    static void clear(QQmlListProperty<QAbstractItemModel>* list);

    QAbstractItemModel* _first{nullptr};
    QAbstractItemModel* _second{nullptr};

    // QAbstractItemModel interface
public:
    QHash<int, QByteArray> roleNames() const;
};

//#endif
}
