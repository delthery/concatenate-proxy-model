import QtQuick 2.12

ListModel {
    id: model
    default property list<ListModel> otherModels

    onOtherModelsChanged: {
        var i = otherModels.length - 1;
        if (i < 0) return;

        for (var j = 0; j < otherModels[i].count; j++)
            model.append(otherModels[i].get(j));    }
}
